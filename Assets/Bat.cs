﻿using NeuralNetwork;
using System.Collections;
using System.Collections.Generic;
using TimGameV2.Engine;
using UnityEngine;

public class Bat : MonoBehaviour
{
    private ReinforcedAgent agent;
    [SerializeField] private Ball ball;

    [SerializeField] private TextMesh positiveText, negativeText;

    private string brainName
    {
        get
        {
            return transform.position.x > 0 ? "RightBrain.brain" : "LeftBrain.brain";
        }
    }

	// Use this for initialization
	void Start ()
    {
        if (agent == null)
        {
            BrainBox box = DataSaver.LoadFile<BrainBox>(brainName);

            if (box != null)
            {
                agent = NeuralNetwork.ReinforcedAgent.Parse(box.agent);
                Debug.Log("loaded brain " + brainName);
            }

            if (agent == null)
            {
                NeuralNetwork.Network brain = new NeuralNetwork.Network(5);
                brain.AddLayer(5, NeuralNetwork.Neuron.ActivationTypes.Linear);
                brain.AddLayer(5, NeuralNetwork.Neuron.ActivationTypes.Linear);
                brain.AddLayer(1);

                agent = new ReinforcedAgent(brain, 10, 6);
                Debug.Log("Did not load brain " + brainName);
            }
        }
        
	}

    void OnApplicationQuit()
    {

        BrainBox box = new BrainBox();

        box.agent = agent.ToString();

        box.SaveAsFile(brainName);
    }

    public void Hit()
    {
        agent.Reward(1);
    }

    public void LosePoint()
    {
        agent.Punish(3);
    }

    public void ScorePoint()
    {
        agent.Reward(2);
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        positiveText.text = agent.Evolutions.ToString();
        negativeText.text = agent.Resets.ToString();

        double[] inputs = new double[]
        {
            ball.transform.position.x,
            ball.transform.position.y,
            ball.GetComponent<Rigidbody2D>().velocity.x,
            ball.GetComponent<Rigidbody2D>().velocity.y,
            transform.position.y
        };

        transform.position += new Vector3(0, Mathf.Clamp((float)agent.GetOutput(inputs)[0], -.2f, .2f), 0);

        if(transform.position.y > 4.13f)
        {
            transform.position += Vector3.down * Mathf.Abs(transform.position.y - 4.13f);
        }

        if (transform.position.y <-4.13f)
        {
            transform.position += Vector3.up * Mathf.Abs(transform.position.y + 4.13f);
        }
    }

    [System.Serializable]
    public class BrainBox
    {
        public string agent;
    }
}
