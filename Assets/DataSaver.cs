﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace TimGameV2.Engine
{
    static class DataSaver
    {
        public static string SaveFolder
        {
            get
            {
                if (saveFolder == "")
                    saveFolder = DefaultSaveFolder;

                return saveFolder;
            }

            set
            {
                saveFolder = value;
            }
        }

        private static string DefaultSaveFolder
        {
            get
            {
                return Application.dataPath + saveFolderName;
            }
        }

        private static string saveFolder = "";

        private static string saveFolderName = "\\Saves";

		public static string[] Files
        {
            get
            {
                string[] files = System.IO.Directory.GetFiles(SaveFolder);
 
                for(int i = 0; i < files.Length; i++)
                {
                    files[i] = files[i].Replace(SaveFolder + "\\", "");
                }

                return files;
            }
        }

        public static void DeleteFile(string fileName)
        {
            if (Directory.Exists(SaveFolder))
            {
                if (File.Exists(SaveFolder + "\\" + fileName))
                {
                    File.Delete(SaveFolder + "\\" + fileName);
                }
            }
        }
		
        public static bool SaveAsFile<T>(this T data, string fileName)
        {
            Debug.Log("Saving File: " + SaveFolder + "\\" + fileName);
            
            string jsonData = JsonUtility.ToJson(data);

            bool canDo = true;

            if(!Directory.Exists(SaveFolder))
            {
                DirectoryInfo info = Directory.CreateDirectory(SaveFolder);
                canDo = info.Exists;
            }

            if(canDo)
            {
                StreamWriter writer = File.CreateText(SaveFolder + "\\" + fileName);
                writer.Write(jsonData);
                writer.Close();

                return true;
            }

            return false;
        }

        public static T LoadFile<T>(string fileName)
        {
            Debug.Log("Loading File: " + SaveFolder + "\\" + fileName);
            
            if (!Directory.Exists(SaveFolder))
            {
                return default(T);
            }
            else
            {
                if(File.Exists(SaveFolder + "\\" + fileName))
                {
                    StreamReader reader = File.OpenText(SaveFolder + "\\" + fileName);
                    string json = reader.ReadToEnd();
					reader.Close();
                    
                    return JsonUtility.FromJson<T>(json);
                }
            }

            return default(T);
        }
    }
}
