﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        GetComponent<Rigidbody2D>().AddForce(Random.insideUnitCircle * 40);
	}
	
	// Update is called once per frame
	void Update () {
		if(Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) < 7)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(GetComponent<Rigidbody2D>().velocity.x, 0));
        }

        if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.y) < 2 && Mathf.Abs(transform.position.y) > 4)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -transform.position.y));
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.GetComponent<Goal>() != null)
        {
            collision.gameObject.GetComponent<Goal>().Score();

            transform.position = Vector3.zero;
            Start();
        }

        if (collision.gameObject.GetComponent<Bat>() != null)
        {
            collision.gameObject.GetComponent<Bat>().Hit();
        }
    }
}
