﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour {

    [SerializeField] private Bat myBat, enemyBat;

    public void Score()
    {
        myBat.LosePoint();
        //enemyBat.ScorePoint();
    }
}
